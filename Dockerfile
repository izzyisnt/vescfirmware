FROM phusion/baseimage:dev-jammy-1.1.0
ENV DEBIAN_FRONTEND noninteractive

# Clone the repo
RUN mkdir /usr/local/vesc
WORKDIR /usr/local/vesc
RUN apt-get -y update && apt-get -y install build-essential git
RUN git clone git@github.com:vedderb/vesc-os-pi.git
WORKDIR vesc-os-pi
RUN git submodule update --init
WORKDIR buildroot

# Build for Raspberry Pi 3 using 12 cores
RUN make BR2_EXTERNAL=../ vesc_rpi3_defconfig
RUN make BR2_EXTERNAL=../ BR2_JLEVEL=12

# podman cp foo:buildroot/output/images/sdcard.img .
# sudo dd if=output/images/sdcard.img of=/dev/[card_id] bs=1M

RUN echo "export PS1=vescpi%\ " >> ~/.bashrc
ENTRYPOINT [ "bash"]
