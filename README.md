<a href=".">
  <img src="img/logo.png" width="300" height="300">
</a>

## Build DC/BLDC/FOC controller firmware from https://github.com/vedderb/vesc
<br>
75% of electrical energy is consumed in generating torque. Lowering the barrier to entry for utilizing efficient variable speed software commutated machines is a means for increasing adoption of the better way to make torque. The method here is the most straight forward means for building custom firmware for any VESC hardware. Various modular means of building VESC powered machines will be gathered here with proof of concept parts inventoried and documented at the electric rod design lab at Tinkmill, Longmont CO, USA. The default hardware is Shaman Systems Cheap FOC, which has brought the BOM down by a factor of 10. The VESC code includes balance app, blinky light driver, dash/gauges, CAN/UART//NRF remote, and realtime LISP scripting.
<br>
<br>

## Dependencies
```
reboot
brew install podman or equivalent, e.g: https://docs.docker.com/desktop/windows/install
podman machine start
```

## Show possible build targets
```
podman run -it milfordtrout/vesc | grep supported
```

## Build firmware for mbot board and harvest result from vesc devenv Ubuntu container
```
podman run -it --name vesc milfordtrout/vesc mbot
podman cp vesc:build .
```

## Cleanup
```
podman container flush; podman image rm -f vesc ; podman image ls ; podman ps -a
```

# Generate image for RPI dashboard
Starts VESC Tool in Qml UI mode. It is also possible to start the mobile version or the full desktop version by using one of the following configs (depending on your raspberry pi version)
```
podman run -it --name vescpi milfordtrout/vescpi
```

<BR>
<BR>
<BR>

## VESC Community Projects
[Shaman Systems Cheap Focer](https://github.com/shamansystems/Cheap-FOCer-2) <br>
[VESC tool on RPI](https://github.com/vedderb/vesc-os-pi)
[30kw](https://vesc-project.com/node/1477)
[Simple ESP32 Display](https://github.com/SimonRafferty/VESC_ESP32_Display)
<a href="https://github.com/SimonRafferty/VESC_ESP32_Display">
  <img src="img/display.png" width="80" height="40">
</a> <br>
[UART lib with CAN](https://github.com/SolidGeek/VescUart) <br>
[CAN DBC database](https://gitlab.com/jonasbareiss/vesc-dbc) <br>
[Funwheel support](https://github.com/thankthemaker/rESCue)
<a href="https://rescue.company.site/">
  <img src="img/rescue.png" width="30" height="30">
</a> <br>
[Multiple VESCs via Teensy](https://github.com/sangvikh/mcp_vesc_can) <br>
[E-bike Tachometer](https://github.com/roland-burke/arduino-vesc-tachometer) <br>
[Simple Throttle Control](https://github.com/KeiranHines/VescThrottleController) <br>
[stm32CubeIDE C library for interfacing with a VESC over UART](https://github.com/HYEDO-KIM/VescUart) <br>
[VESC repos](https://github.com/search?p=2&q=vesc&type=Repositories) <br>

